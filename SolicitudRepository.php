<?php

namespace App\Repositories;

use App\User;
use App\Solicitud;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SolicitudRepository implements SolicitudInterface{

    public function getAllSolicitud() {
        return Solicitud::with('proyecto','owner')->allowed()->get();
    }

    public function store($request){
        $request->validate([
            'codigo_solicitud'=> 'required'
        ],[
            'codigo_solicitud.required'=> 'El campo es obligatorio'
        ]);

        $solicitud = new Solicitud;

        $solicitud->codigo = $request->get('codigo_solicitud');
        $solicitud->fecha_solicitada = date('Y-m-d');

        $solicitud->user_id = auth()->id();

        $solicitud->save();

        return $solicitud;
    }

    public function edit($solicitud){
        $user = User::find($solicitud->user_id);
        return ["solicitud" => $solicitud,"owner"=> $solicitud->owner,"saldo_pendiente" => $user->datos['saldo_inicial'] ?? '',"saldo_proceso"=>($user->datos['saldo_proceso'] ?? '')];
    }

    public function update($request,$solicitud,$data){
        // dd($data);
        $solicitud->fill($data);

        $jsonData = $solicitud->datos;
        // datos en el json
        if($request->exists('encargado')){
            $jsonData['encargado'] = $request->get('encargado') ?? '';
            $solicitud->encargado_id = $request->get('encargado') ?? '';
        }

        $solicitud->datos = $jsonData;
        // estado
        if($solicitud->user_id == auth()->id()){
            if($solicitud->observed == 1){
                $solicitud->observed = 0;
            }else{
                if(auth()->user()->hasRole('Project')){
                    $solicitud->estado = 2;
                }elseif(auth()->user()->hasRole('Controller')){
                    $solicitud->estado = 3;
                }elseif(auth()->user()->hasRole('Manager')){
                    $solicitud->estado = 4;
                }else{
                    $solicitud->estado = 1;
                }
            }
        }
        // actualizamos
        $solicitud->update();

        // validamos los montos
        if(auth()->user()->can('special', $solicitud) && $solicitud->estado == '4'){
            $usuario = User::find($solicitud->user_id);
            $jsonData = $usuario->datos;
            foreach($jsonData['saldo_solicitud'] ?? [] as $key => $saldo){
                if($saldo['id'] == $solicitud->id && $saldo['monto'] != $solicitud->monto_aprobado){
                    $monto_sumar = $saldo['monto'] - $solicitud->monto_aprobado;
                    $jsonData["saldo_inicial"] = $jsonData["saldo_inicial"] - $monto_sumar;
                    $jsonData['saldo_solicitud'][$key]['monto'] = $solicitud->monto_aprobado;
                }
            }
            $usuario->datos = $jsonData;
            $usuario->save();
        }
        $solicitud->syncSites($request->get('sites'));

        return $solicitud;
    }

    public function deleteItem($request, $solicitud){

        $id = $request->get('delete_id');
        $data =  $solicitud->datos;

        $montoTotal = 0;
        foreach($data['items'] as $key => $valor){
            if($valor['id'] == $id){
                unset($data['items'][$key]);
            }else{
                $montoTotal += $valor['cantidad'] * $valor['cantidadUnitaria'];
            }
        }
        $solicitud->monto_aprobado = $montoTotal;
        $solicitud->monto_solicitado = $montoTotal;

        $solicitud->datos = $data;
        $solicitud->save();

        return $solicitud;
    }

    public function createItem($request,$solicitud){
        $jsonDatos = [];

        if(!empty($solicitud->datos)){
            $jsonDatos = $solicitud->datos;
        }
        $jsonDatos['items'][] = $request->all();

        $solicitud->datos = $jsonDatos;
        $montoTotal = 0;
        foreach($jsonDatos['items'] ?? [] as $item){
            $montoTotal += $item['cantidad'] * $item['cantidadUnitaria'];
        }
        $solicitud->monto_aprobado = $montoTotal;
        $solicitud->monto_solicitado = $montoTotal;

        $solicitud->save();

        return $solicitud;
    }

    public function storeDocument($request, $solicitud){
        $request->validate([
            'file' => 'mimes:jpg,jpeg,bmp,png,doc,docx,csv,xlsx,xls,txt,pdf,zip,rar'
        ],
        [
            'file.mimes' => 'El archivo no cumple con el formato requerido'
        ]);
        $file = $request->file('file');

        $url = $file->store('solicitud','public');

        $jsonData = $solicitud->datos;

        $jsonData['docs'][] = ["path"=>$url,"name"=>$file->getClientOriginalName(),"ext"=>$file->getClientOriginalExtension(),"size"=>$file->getSize(),"fecha"=> Carbon::now()];

        $solicitud->datos = $jsonData;

        $solicitud->save();

        return $solicitud;
    }

    public function destroyDocument($request,$solicitud){

        $path = $request->get('path');
        $data =  $solicitud->datos;
        foreach($data['docs'] as $key => $valor){
            if($valor['path'] == $path){
                Storage::disk('public')->delete($valor['path']);
                unset($data['docs'][$key]);
            }
        }
        $solicitud->datos = $data;

        $solicitud->save();

        return $solicitud;
    }
}
