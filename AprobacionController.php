<?php

namespace App\Http\Controllers;

use App\User;
use App\Solicitud;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Notifications\ApprovalSent;
use App\Notifications\PaidSolicitud;
use Illuminate\Support\Facades\Cache;

class AprobacionController extends Controller
{
    public function index(Solicitud $solicitud)
    {
        return view('solicitud.validation',compact('solicitud'));
    }
    public function store(Request $request,Solicitud $solicitud){
        $data = $request->validate([
            'estado' => 'required',
        ]);
        $aprobacion = ["user" => auth()->user(),"estado"=>$request->get('estado'),"comentario"=>$request->get('comentario') ?? '','fecha'=>Carbon::now(),'role'=>auth()->user()->getRoleDisplayNames()];

        $jsonData = $solicitud->datos;
        $jsonData['aprobaciones'][] = $aprobacion;

        $solicitud->datos = $jsonData;
        if($request->get('estado') != '0'){
            $solicitud->observed = 1;
        }else{
            if($solicitud->observed == 1){
                $solicitud->observed = 0;
            }else{
                if(auth()->user()->hasRole('Project')){
                    $solicitud->estado = 2;
                }elseif(auth()->user()->hasRole('Controller')){
                    $solicitud->estado = 3;
                }elseif(auth()->user()->hasRole('Manager')){
                    $solicitud->estado = 4;
                }elseif(auth()->user()->hasRole('Manager')){
                    $solicitud->estado = 4;
                }
            }
        }
        $solicitud->save();

        if($request->get('estado') != '0' || $solicitud->estado == 4){
            $recipient = User::find($solicitud->user_id);
            // $mail_project = (auth()->id() == $solicitud->encargado_id) ? null: User::find($solicitud->encargado_id)->email ?? null;
            // $recipient->notify(
            //     new ApprovalSent(
            //         $recipient,
            //         $solicitud,
            //         auth()->user(),
            //         $request->get('comentario'),
            //         auth()->user()->getRoleDisplayNames(),
            //         $mail_project
            //     )
            // );
            // if($solicitud->estado == 5){
            //     $jsonData = $recipient->datos;
            //     if(empty($jsonData['saldo_solicitud'] ?? [])){
            //         $jsonData["saldo_solicitud"][] = ["id" => $solicitud->id, "monto"=> $solicitud->monto_aprobado];
            //         $jsonData["saldo_inicial"] = ($jsonData["saldo_inicial"] ?? 0) + $solicitud->monto_aprobado;
            //     }else{
            //         foreach($jsonData['saldo_solicitud'] ?? [] as $saldo){
            //             if($saldo['id'] != $solicitud->id){
            //                 $jsonData["saldo_solicitud"][] = ["id" => $solicitud->id, "monto"=> $solicitud->monto_aprobado];
            //                 $jsonData["saldo_inicial"] = ($jsonData["saldo_inicial"] ?? 0) + $solicitud->monto_aprobado;
            //             }
            //         }
            //     }
            //     $recipient->datos = $jsonData;
            //     $recipient->save();
            // }
        }
        Cache::tags("solicitud")->flush();

        return redirect()->route('solicitud.edit',$solicitud)->withFlash('Su aprobación fue exitosa');
    }
    public function end(Solicitud $solicitud){
        request()->validate([
            'estado' => 'required',
        ]);

        $jsonData = $solicitud->datos;

        $jsonData['estado_pagado'] = request()->get('estado');

        $solicitud->datos = $jsonData;

        $solicitud->update();

        Cache::tags("solicitud")->flush();

        if($solicitud->estado == 4 && request()->get('estado') == '1'){
            $recipient = User::find($solicitud->user_id);
            $mail_project = (auth()->id() == $solicitud->encargado_id) ? null: User::find($solicitud->encargado_id)->email ?? null;
            $recipient->notify(
                new PaidSolicitud(
                    $recipient,
                    $solicitud,
                    $mail_project
                )
            );
            if($solicitud->estado == 4){
                $jsonData = $recipient->datos;
                if(empty($jsonData['saldo_solicitud'] ?? [])){
                    $jsonData["saldo_solicitud"][] = ["id" => $solicitud->id, "monto"=> $solicitud->monto_aprobado];
                    $jsonData["saldo_inicial"] = ($jsonData["saldo_inicial"] ?? 0) + $solicitud->monto_aprobado;
                }else{
                    $arr = [];
                    foreach($jsonData['saldo_solicitud'] ?? [] as $saldo){
                        $arr[] = $saldo['id'];
                    }
                    if(!collect($arr)->contains($solicitud->id)){
                        $jsonData["saldo_solicitud"][] = ["id" => $solicitud->id, "monto"=> $solicitud->monto_aprobado];
                        $jsonData["saldo_inicial"] = ($jsonData["saldo_inicial"] ?? 0) + $solicitud->monto_aprobado;
                    }
                }
                $recipient->datos = $jsonData;
                $recipient->update();
            }
        }else{
            $recipient = User::find($solicitud->user_id);
            $jsonData = $recipient->datos;
            $jsonData["saldo_inicial"] = ($jsonData["saldo_inicial"] ?? 0) - $solicitud->monto_aprobado;
            $recipient->datos = $jsonData;
            $recipient->update();
        }
    }
}
