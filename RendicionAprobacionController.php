<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Rendicion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Notifications\ApprovalSentRendicion;

class RendicionAprobacionController extends Controller
{
    public function index(Rendicion $rendicion)
    {
        return view('rendicion.validation',compact('rendicion'));
    }

    public function store(Request $request,Rendicion $rendicion){
        $data = $request->validate([
            'estado' => 'required',
        ]);
        $aprobacion = ["user" => auth()->user(),"estado"=>$request->get('estado'),"comentario"=>$request->get('comentario') ?? '','fecha'=>Carbon::now(),'role'=>auth()->user()->getRoleDisplayNames()];

        $jsonData = $rendicion->datos;
        $jsonData['aprobaciones'][] = $aprobacion;

        $rendicion->datos = $jsonData;
        if($request->get('estado') != '0'){
            $rendicion->observed = 1;
        }else{
            if($rendicion->observed == 1){
                $rendicion->observed = 0;
            }else{
                if(auth()->user()->hasRole('Project')){
                    $rendicion->estado = 2;
                }elseif(auth()->user()->hasRole('Controller')){
                    $rendicion->estado = 3;
                }elseif(auth()->user()->hasRole('Manager')){
                    $rendicion->estado = 4;
                }elseif(auth()->user()->hasRole('Accountant')){
                    $rendicion->estado = 5;
                }
            }
        }
        $rendicion->save();

        if($request->get('estado') != '0' || $rendicion->estado == 5){
            $recipient = User::find($rendicion->user_id);
            $mail_project = (auth()->id() == $rendicion->encargado_id) ? null: User::find($rendicion->encargado_id)->email ?? null;
            $recipient->notify(
                new ApprovalSentRendicion(
                        $recipient,
                        $rendicion,
                        auth()->user(),
                        $request->get('comentario'),
                        auth()->user()->getRoleDisplayNames(),
                        $mail_project,
                    )
                );
            if($rendicion->estado == 5){
                $jsonData = $recipient->datos;
                $jsonData["saldo_rendicion"][] = ["id" => $rendicion->id, "monto"=> $rendicion->monto_rendido];
                $jsonData["saldo_proceso"] = ($jsonData["saldo_proceso"] ?? 0) - $rendicion->monto_rendido;
                $recipient->datos = $jsonData;
                $recipient->save();
            }
        }

        Cache::tags('rendicion')->flush();

        return redirect()->route('rendicion.edit',$rendicion)->withFlash('Su aprobación fue exitosa');
    }
}
