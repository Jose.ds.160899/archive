<?php

namespace App\Http\Controllers;

use App\User;
use App\Events\UserWasCreated;
use App\Events\UserSaved;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::allowed()->get();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',new User);
        $user = new User;
        $roles = Role::with('permissions')->get();
        $permissions = Permission::all();
        Cache::flush('users');
        return view('users.create',compact('user','roles','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',new User);
        // validamos los datos
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
        // generamos la contraseña encriptada
        $data['password'] = $password = 's3m1p3ru';
        // creamos el usuario
        $user = User::create($data);


        // asignamos los roles
        if($request->filled('roles')){
            $user->assignRole($request->roles);
        }
        // asignamos los permisos
        if($request->filled('permissions')){
            $user->givePermissionTo($request->permissions);
        }
        // enviamos al usuario un email con sus credenciales
        Cache::flush('users');
        UserWasCreated::dispatch($user, $data['password']);
        return redirect()->route('users.index')->withFlash('El usuario ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('view',$user);
        return view('users.show',['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update',$user);
        // $roles = Role::all()->pluck('name','id');
        $roles = Role::with('permissions')->get();
        $permissions = Permission::all();
        Cache::flush('users');
        return view('users.edit',compact('user','roles','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request,User $user)
    {
        // dd($request);
        $this->authorize('update',$user);

        if($request->hasFile('foto')){
            Storage::disk('public')->delete($user->foto);

            $user->fill($request->validated());

            $user->foto = $request->file('foto')->store('users','public');

            $user->save();

            UserSaved::dispatch($user);
        }else{
            $user->update( array_filter($request->validated()));
        }
        Cache::flush('users');
        return redirect()->route('users.edit',$user)->withFlash('Usuario Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete',$user);

        $user->delete();
        Cache::flush('users');
        return redirect()->route('users.index')->withFlash('El usuario ah sido eliminado');
    }
    public function adicional(Request $request,User $user)
    {
        $data = $request->validate([
            'codigo_user' => 'required',
            'documento_identidad' => 'required',
            'cuenta_bancaria' => 'required',
            'codigo_interbancario' => 'required',
            'banco' => 'required',
        ]);

        $jsonData = $user->datos;
        $jsonData['codigo_user'] = $request->get('codigo_user');
        $jsonData['documento_identidad'] = $request->get('documento_identidad');
        $jsonData['cuenta_bancaria'] = $request->get('cuenta_bancaria');
        $jsonData['codigo_interbancario'] = $request->get('codigo_interbancario');
        $jsonData['banco'] = $request->get('banco');
        if($request->has('saldo_inicial')){
            $jsonData['saldo_inicial'] = $request->get('saldo_inicial');
        }
        $jsonData['saldo_arranque'] = $request->get('saldo_arranque');
        $jsonData['fecha_arranque'] = $request->get('fecha_arranque');

        $user->datos = $jsonData;
        $user->update();
        Cache::flush('users');
        Cache::tags('solicitud')->flush();
        Cache::tags('rendicion')->flush();
        return redirect()->route('users.show',$user)->withFlash('Se actualizo correctamente los datos');
    }
}
