@extends('layout')

@section('content')
<div class="row gy-4">
    <div class="col-12 col-lg-6">
        <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
            <div class="app-card-header p-3 border-bottom-0">
                <div class="row align-items-center gx-3">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                            </svg>
                        </div><!--//icon-holder-->

                    </div><!--//col-->
                    <div class="col-auto">
                        <h4 class="app-card-title">Perfil</h4>
                    </div><!--//col-->
                </div><!--//row-->
            </div><!--//app-card-header-->
            <div class="app-card-body px-4 w-100">
                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label mb-2"><strong>Foto</strong></div>
                            <div class="item-data">
                                @if ($user->foto)
                                    <img class="profile-image" src="{{ '/storage/'.$user->foto }}" alt="" style="object-fit:cover;">
                                @else
                                    <img class="profile-image" src="/images/guest.png" alt="" style="object-fit:cover;">
                                @endif
                            </div>
                        </div><!--//col-->
                        <div class="col text-end">
                            {{-- <a class="btn-sm app-btn-secondary" href="#">Change</a> --}}
                        </div><!--//col-->
                    </div><!--//row-->
                </div><!--//item-->
                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label"><strong>Nombre</strong></div>
                            <div class="item-data">{{ $user->name }}</div>
                        </div><!--//col-->
                        <div class="col text-end">
                            {{-- <a class="btn-sm app-btn-secondary" href="#">Change</a> --}}
                        </div><!--//col-->
                    </div><!--//row-->
                </div><!--//item-->
                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label"><strong>Correo Electrónico</strong></div>
                            <div class="item-data">{{ $user->email }}</div>
                        </div><!--//col-->
                        <div class="col text-end">
                            {{-- <a class="btn-sm app-btn-secondary" href="#">Change</a> --}}
                        </div><!--//col-->
                    </div><!--//row-->
                </div><!--//item-->

                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label"><strong>Roles</strong></div>
                            <div class="item-data">
                                @forelse ($user->roles as $role)
                                    {{ $role->name }}
                                    @if ($role->permissions->count())
                                        <br>
                                        <small class="text-muted">
                                            Permisos: {{ $role->permissions->pluck('display_name')->implode(', ') }}
                                        </small>
                                    @endif
                                    @unless ($loop->last)
                                        <hr>
                                    @endunless
                                @empty
                                    <small class="text-muted">No tiene ningún rol asignado</small>
                                @endforelse
                            </div>
                        </div><!--//col-->
                        <div class="col text-end">

                        </div><!--//col-->
                    </div><!--//row-->
                </div><!--//item-->

                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label"><strong>Permisos Adicionales</strong></div>
                            <div class="item-data">
                                @forelse ($user->permissions as $permission)
                                    {{ $permission->display_name }}
                                    @unless ($loop->last)
                                        ,
                                    @endunless
                                @empty
                                    <small class="text-muted">No tiene permisos adicionales</small>
                                @endforelse
                            </div>
                        </div><!--//col-->
                    </div><!--//row-->
                </div>
            </div><!--//app-card-body-->
            <div class="app-card-footer p-4 mt-auto">
               <a class="btn app-btn-secondary" href="{{ route('users.edit',$user) }}">Editar Perfil</a>
            </div><!--//app-card-footer-->

        </div><!--//app-card-->
    </div><!--//col-->
    <div class="col-12 col-lg-6">
        <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
            <div class="app-card-header p-3 border-bottom-0">
                <div class="row align-items-center gx-3">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-sliders" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"/>
                            </svg>
                        </div><!--//icon-holder-->

                    </div><!--//col-->
                    <div class="col-auto">
                        <h4 class="app-card-title">Datos Adicionales</h4>
                    </div><!--//col-->
                </div><!--//row-->
            </div><!--//app-card-header-->
            <form method="GET" action="{{ route('users.adicional',$user) }}">
                @csrf
                <div class="app-card-body px-4 w-100">
                    <div class="item border-bottom py-3">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <div class="item-label"><strong>Código</strong></div>
                                <input type="text" class="form-control @error('codigo_user') is-invalid @enderror" name="codigo_user" placeholder="Ingresa las iniciales de tu nombre" style="width: 400px; margin-top: .600rem; border:none;" value="{{ old('codigo_user',$user->datos['codigo_user'] ?? '') }}">
                                @error('codigo_user')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="item border-bottom py-3">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <div class="item-label"><strong>DNI / RUC </strong></div>
                                <input type="text" class="form-control @error('documento_identidad') is-invalid @enderror" name="documento_identidad" placeholder="Ingresa tu DNI / RUC" style="width: 400px; margin-top: .600rem; border:none;" value="{{ old('documento_identidad',$user->datos['documento_identidad'] ?? '') }}">
                                @error('documento_identidad')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="item border-bottom py-3">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <div class="item-label"><strong>Número de Cuenta Bancaria</strong></div>
                                <input type="text" class="form-control @error('cuenta_bancaria') is-invalid @enderror" name="cuenta_bancaria" placeholder="Ingresa tu número de cuenta" style="width: 400px; margin-top: .600rem; border:none;" value="{{ old('cuenta_bancaria',$user->datos['cuenta_bancaria'] ?? '') }}">
                                @error('cuenta_bancaria')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div><!--//col-->

                        </div><!--//row-->
                    </div><!--//item-->
                    <div class="item border-bottom py-3">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <div class="item-label"><strong>Código Interbancario</strong></div>
                                <input type="text" class="form-control @error('codigo_interbancario') is-invalid @enderror" name="codigo_interbancario" placeholder="Ingresa tu CI" style="width: 400px; margin-top: .600rem; border:none;" value="{{ old('codigo_interbancario',$user->datos['codigo_interbancario'] ?? '') }}">
                                @error('codigo_interbancario')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div><!--//col-->
                        </div><!--//row-->
                    </div><!--//item-->
                    <div class="item border-bottom py-3">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <div class="item-label"><strong>Banco</strong></div>
                                <select id="inputState" class="form-control @error('banco') is-invalid @enderror" name="banco" style="width:100%">
                                    <option value="" selected hidden>Selecciona tu Banco</option>
                                    @foreach (collect(['bpc'=>'BCP','interbank'=>'Interbank','bbva'=>'BBVA','scotibank'=>'Scotiabank']) as $key => $banca)
                                        <option value="{{ $key }}" {{ (old('banco',$user->datos['banco'] ?? '')) == $key ? 'selected':'' }}>{{ $banca }}</option>
                                    @endforeach
                                </select>
                                @error('banco')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div><!--//col-->

                        </div><!--//row-->
                    </div><!--//item-->
                    @can('special', $user)
                        <div class="item border-bottom py-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="item-label"><strong>Saldo Actual</strong></div>
                                    <input type="text" class="form-control @error('saldo_inicial') is-invalid @enderror" name="saldo_inicial" placeholder="0.00" style="width: 400px; margin-top: .600rem; border:none;" value="{{ setNumberFormat(old('saldo_inicial',$user->datos['saldo_inicial'] ?? '0.00')) }}">
                                    @error('saldo_inicial')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                            </div>
                        </div>

                        <div class="item border-bottom py-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="item-label"><strong>Saldo Arranque</strong></div>
                                    <input type="text" class="form-control @error('saldo_arranque') is-invalid @enderror" name="saldo_arranque" placeholder="0.00" style="width: 400px; margin-top: .600rem; border:none;" value="{{ setNumberFormat(old('saldo_arranque',$user->datos['saldo_arranque'] ?? '0.00')) }}">
                                    @error('saldo_arranque')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="item border-bottom py-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="item-label"><strong>Fecha Arranque</strong></div>
                                    <input type="date" class="form-control @error('fecha_arranque') is-invalid @enderror" name="fecha_arranque" placeholder="0.00" style="width: 400px; margin-top: .600rem; border:none;" value="{{ old('fecha_arranque',$user->datos['fecha_arranque'] ?? '2021-10-01') }}">
                                    @error('fecha_arranque')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    @endcan

                </div><!--//app-card-body-->
                <div class="app-card-footer p-4 mt-auto">
                   <button class="btn app-btn-secondary">Actualizar</button>
                </div><!--//app-card-footer-->
            </form>

        </div><!--//app-card-->
    </div><!--//col-->
    <div class="col-12 col-lg-6">
        <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
            <div class="app-card-header p-3 border-bottom-0">
                <div class="row align-items-center gx-3">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-files" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4 2h7a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H4z"/>
                                <path d="M6 0h7a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2v-1a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1H4a2 2 0 0 1 2-2z"/>
                            </svg>
                        </div><!--//icon-holder-->
                    </div><!--//col-->
                    <div class="col-auto">
                        <h4 class="app-card-title">Solicitudes</h4>
                    </div><!--//col-->
                </div><!--//row-->
            </div><!--//app-card-header-->
            <div class="app-card-body px-4 w-100">
                @forelse ($user->Solicitudes as $solicitud)
                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label"><strong>{{ $solicitud->codigo }}</strong></div>
                            <div class="item-data">Solicitado: {{ optional($solicitud->fecha_solicitada)->diffForHumans() }}</div>
                        </div><!--//col-->
                        <div class="col text-end">
                            <a class="btn-sm app-btn-secondary" href="{{ route('solicitud.edit',$solicitud) }}" target="_blank">Editar</a>
                        </div><!--//col-->
                    </div><!--//row-->
                </div><!--//item-->
                @empty
                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label">No tiene ninguna solicitud</div>
                        </div>
                    </div>
                </div>
                @endforelse
            </div><!--//app-card-body-->
        </div><!--//app-card-->
    </div>
    <div class="col-12 col-lg-6">
        <div class="app-card app-card-account shadow-sm d-flex flex-column align-items-start">
            <div class="app-card-header p-3 border-bottom-0">
                <div class="row align-items-center gx-3">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-card-list" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                            <path fill-rule="evenodd" d="M5 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 5 8zm0-2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5z"/>
                            <circle cx="3.5" cy="5.5" r=".5"/>
                            <circle cx="3.5" cy="8" r=".5"/>
                            <circle cx="3.5" cy="10.5" r=".5"/>
                            </svg>
                        </div><!--//icon-holder-->

                    </div><!--//col-->
                    <div class="col-auto">
                        <h4 class="app-card-title">Rendiciones</h4>
                    </div><!--//col-->
                </div><!--//row-->
            </div><!--//app-card-header-->
            <div class="app-card-body px-4 w-100">
                <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label">No tiene ninguna Rendición</div>
                        </div>
                    </div>
                </div>
                {{-- <div class="item border-bottom py-3">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="item-label"><i class="fab fa-cc-visa me-2"></i><strong>Rendicion 1 </strong></div>
                            <div class="item-data">Aprobado: </div>
                        </div>
                        <div class="col text-end">
                            <a class="btn-sm app-btn-secondary" href="#">Edit</a>
                        </div>
                    </div>
                </div> --}}

            </div><!--//app-card-body-->
        </div><!--//app-card-->
    </div>
</div><!--//row-->
@endsection
