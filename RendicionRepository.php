<?php


namespace App\Repositories;

use App\User;
use Carbon\Carbon;
use App\Rendicion;
use App\Events\SurrenderSaved;
use Illuminate\Support\Facades\Storage;


class RendicionRepository implements RendicionInterface{
    function getAllRendicion(){
        return Rendicion::with('proyecto','owner')->allowed()->get();
    }

    function store($request){
        $request->validate([
            'codigo_rendicion'=> 'required'
        ],[
            'codigo_rendicion.required'=> 'El campo es obligatorio'
        ]);

        $rendicion = new Rendicion;

        $rendicion->codigo = $request->get('codigo_rendicion');
        $rendicion->fecha_rendida = date('Y-m-d');

        $rendicion->user_id = auth()->id();

        $rendicion->save();

        return $rendicion;
    }

    function edit($rendicion){
        return ["rendicion" => $rendicion,"owner"=> $rendicion->owner];
    }

    function update($request,$rendicion){

        $rendicion->fill($request->all());

        $jsonData = $rendicion->datos;
        // datos en el json
        if($request->exists('encargado')){
            $jsonData['encargado'] = $request->get('encargado') ?? '';
            $rendicion->encargado_id = $request->get('encargado') ?? '';
        }

        $rendicion->datos = $jsonData;
        // estado
        if($rendicion->user_id == auth()->id()){
            if($rendicion->observed == 1){
                $rendicion->observed = 0;
            }else{
                if(auth()->user()->hasRole('Project')){
                    $rendicion->estado = 2;
                }elseif(auth()->user()->hasRole('Controller')){
                    $rendicion->estado = 3;
                }elseif(auth()->user()->hasRole('Manager')){
                    $rendicion->estado = 4;
                }else{
                    $rendicion->estado = 1;
                }
            }
        }
        if($rendicion->estado != 5){
            $usuario = User::find($rendicion->user_id);
            $jsonData = $usuario->datos;
            if(empty($jsonData["proceso_rendicion"] ?? [])){
                $jsonData["proceso_rendicion"][] = ["id" => $rendicion->id, "monto"=>$rendicion->monto_rendido];
                $jsonData["saldo_proceso"] = ($jsonData["saldo_proceso"] ?? 0) + $rendicion->monto_rendido;
            }else{
                $arr = [];
                foreach($jsonData['proceso_rendicion'] as $key => $proceso){
                    if($rendicion->id == $proceso['id']){
                        $monto = $jsonData["proceso_rendicion"][$key]["monto"];
                        $jsonData["proceso_rendicion"][$key]["monto"] = $rendicion->monto_rendido;
                        $jsonData["saldo_proceso"] = (($jsonData["saldo_proceso"] ?? 0) - $monto) + $rendicion->monto_rendido;
                        $arr[] = $proceso['id'];
                    }
                }
                if(!collect($arr)->contains($rendicion->id)){
                    $jsonData["proceso_rendicion"][] = ["id" => $rendicion->id, "monto"=>$rendicion->monto_rendido];
                    $jsonData["saldo_proceso"] = ($jsonData["saldo_proceso"] ?? 0) + $rendicion->monto_rendido;
                }
                // dd($arr);
            }
            $usuario->datos = $jsonData;
            $usuario->save();
        }
        // actualizamos
        $rendicion->update();

        return $rendicion;
    }
    function importItem($request, $rendicion,$rendicionImport){

        $request->validate(
            [
                'file' => 'mimes:xlsx,xlsm',
                'tipo' => 'required',
            ],
            [
                'file.mimes' => 'El archivo no cumple con el formato requerido'
            ]
        );
        $mensaje = 200;
        $tipo = $request->get('tipo');
        $file = $request->file('file');
        $path = $file->storeAs('import',$rendicion->id.'.xlsx','public');

        $array = $rendicionImport->toArray($file)[0];

        $montoTotal = $montoLote1 = $montoLote2 = 0;
        $lote1 = $lote2 = array();

        if(($tipo ?? 'P') == 'P' || ($tipo ?? 'M') == 'M'){
            if(($array[0][8] ?? '') ==  'MONTO'){
                foreach($array as $key => $item){
                    if($key != 0 && $item[1] == NULL){
                        $lote1[] = $item;
                        $montoLote1 += floatval($item[8]) ?? 0;
                        $montoTotal += floatval($item[8]) ?? 0;
                    }else if($key != 0 && $item[1] != NULL){
                        $lote2[] = $item;
                        $montoLote2 += floatval($item[8]) ?? 0;
                        $montoTotal += floatval($item[8]) ?? 0;
                    }
                }
            }else{
                $mensaje = 405;
            }
        }else{
            if(($array[0][7] ?? '') ==  'MONTO'){
                foreach($array as $key => $item){
                    if($key != 0 && $item[0] != NULL){
                        $lote1[] = ["id"=>uniqid(),"fecha_rendicion"=>$item[0],"proyecto"=>$item[1],"site_pago"=>$item[2],"detalle_gasto"=>$item[3],"descripcion"=>$item[4],"factura"=>$item[5],"razon_social"=>$item[6],"monto"=>$item[7],"status"=>"No gestionado","fecha_gestion"=>"","fecha_pago"=>"","memoramdum"=>"","comentario_project"=>"","comentario_controller"=>""];
                        $montoLote1 += floatval($item[7]) ?? 0;
                        $montoTotal += floatval($item[7]) ?? 0;
                    }
                }
            }else{
                $mensaje = 405;
            }
        }
        // ingresando datos a la DB
        $jsonData = $rendicion->datos;
        $jsonData['lote1'] = $lote1;
        $jsonData['lote2'] = $lote2;
        $jsonData['importacion_excel'] = $path;

        $jsonData['monto_lote1'] = setNumberFormat($montoLote1);
        $jsonData['monto_lote2'] = setNumberFormat($montoLote2);

        $rendicion->datos = $jsonData;
        $rendicion->tipo = $tipo;
        $rendicion->monto_rendido = $montoTotal;
        $rendicion->update();

        $inputProjectDisabled = (auth()->user()->hasRole('Project')) ? '' : 'input-disabled';
        $inputControllerDisabled = (auth()->user()->hasRole('Controller')) ? '' : 'input-disabled';
        return response()->json(['lote1' => $lote1, 'lote2' => $lote2,'monto_rendido' => setNumberFormat($montoTotal),'monto_lote1' => setNumberFormat($montoLote1),'monto_lote2' => setNumberFormat($montoLote2),'mensaje'=>$mensaje,'tipo' => $tipo,"input_project"=>$inputProjectDisabled,'input_controller'=>$inputControllerDisabled,'importacion_excel'=>$path]);
    }
    function updateItem($request, $rendicion){

        $request->validate([
            'id' => 'required',
            'fecha_rendicion' => 'required',
            'proyecto' => 'required',
            'site_pago' => 'required',
            'descripcion' => 'required',
            'factura' => 'required',
            'razon_social' => 'required',
            'monto' => 'required',
        ]);
        $jsonData = $rendicion->datos;
        $monto_total = 0;
        $itemValidate = ["id"=> $request->get('id'),"fecha_rendicion"=>$request->get('fecha_rendicion'),"proyecto"=> $request->get('proyecto'),"site_pago" => $request->get('site_pago'), "descripcion" => $request->get('descripcion'), "factura" => $request->get('factura'),"razon_social"=>$request->get('razon_social'),"monto" => $request->get('monto'),"status"=> $request->get('status'),"fecha_gestion"=> $request->get('fecha_gestion'),"fecha_pago"=>$request->get('fecha_pago'),"memoramdum"=>$request->get('memoramdum'), "detalle_gasto" => $request->get('detalle_gasto'),"comentario_project"=>$request->get('comentario_project'),"comentario_controller" => $request->get('comentario_controller')];
        foreach($jsonData['lote1'] ?? [] as $key => $item){
            if($item['id'] == $request->get('id')){
                $jsonData['lote1'][$key] = $itemValidate;
                $monto_total += $request->get('monto');
            }else{
                $monto_total += $item['monto'];
            }
        }
        $jsonData['monto_lote1'] = $monto_total;
        $rendicion->datos = $jsonData;
        $rendicion->monto_rendido = $monto_total;

        $rendicion->update();

        return response()->json(["monto_total"=>setNumberFormat($monto_total)]);
    }
    function storeDocument($request,$rendicion){

        $request->validate([
            'file' => 'mimes:jpg,jpeg,bmp,png,doc,docx,csv,xlsx,xls,txt,pdf,zip,rar'
        ],
        [
            'file.mimes' => 'El archivo no cumple con el formato requerido'
        ]);

        $file = $request->file('file');
        $url = $file->store('rendicion','public');

        $jsonData = $rendicion->datos;

        $jsonData['docs'][] = ["path"=>$url,"name"=>$file->getClientOriginalName(),"ext"=>$file->getClientOriginalExtension(),"size"=>$file->getSize(),"fecha"=> Carbon::now()];

        $rendicion->datos = $jsonData;

        $rendicion->save();

    }

    function storeDocumentSupport($request, $rendicion)
    {
        $request->validate(
            [
                'file' => 'mimes:pdf'
            ],
            [
                'file.mimes' => 'El archivo no cumple con el formato requerido'
            ]
        );

        $file = $request->file('file');
        $url = $file->store('rendicion','public');

        $jsonData = $rendicion->datos;

        $jsonData['support'][] = ["path"=>$url,"name"=>$file->getClientOriginalName(),"ext"=>$file->getClientOriginalExtension(),"size"=>$file->getSize(),"fecha"=> Carbon::now()];

        $rendicion->datos = $jsonData;

        $rendicion->save();
    }
    function destroyDocument($request,$rendicion){
        $path = $request->get('path');
        $estado = $request->get('estado') ?? 'docs';
        $data =  $rendicion->datos;
        foreach($data[$estado] as $key => $valor){
            if($valor['path'] == $path){
                Storage::disk('public')->delete($valor['path']);
                unset($data[$estado][$key]);
            }
        }
        $rendicion->datos = $data;
        $rendicion->save();

        return $rendicion;
    }
    function mergePdf($rendicion)
    {
        SurrenderSaved::dispatch($rendicion,User::find(auth()->id()));
        return response()->json(['mensaje'=>'Se enviara el link a su correo, para descargar el fichero de sustento unido']);
    }

    function deleteMergePdf($request, $rendicion)
    {
        $path = $request->get('path');
        $data = $rendicion->datos;
        foreach($data['merge_document'] as $key => $valor){
            if($valor['documento'] == $path){
                Storage::disk('public')->delete($valor['documento']);
                unset($data['merge_document'][$key]);
            }
        }
        $rendicion->datos = $data;
        $rendicion->save();

        return $rendicion;
    }
}
